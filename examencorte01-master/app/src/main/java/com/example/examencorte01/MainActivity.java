package com.example.examencorte01;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView lblEncabezado;
    private EditText txtNumCuenta;
    private EditText txtNombre;
    private EditText txtBanco;
    private EditText txtSaldo;
    private Button btnEnviar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enviar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salir();
            }
        });
    }

    private void iniciarComponentes(){
        lblEncabezado=findViewById(R.id.lblEncabezado);
        txtNombre=findViewById(R.id.txtNombre);
        txtBanco=findViewById(R.id.txtBanco);
        txtSaldo=findViewById(R.id.txtSaldo);
        txtNumCuenta=findViewById(R.id.txtNumCuenta);
        btnEnviar= findViewById(R.id.btnEnviar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    private void enviar(){
        String textoNombre = txtNombre.getText().toString().trim();
        String textoBanco = txtBanco.getText().toString().trim();
        String textoSaldo = txtSaldo.getText().toString().trim();
        String textoNumCuenta = txtNumCuenta.getText().toString().trim();

        if (textoNombre.isEmpty()) {
            Toast.makeText(this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
        } else if (textoBanco.isEmpty()){
            Toast.makeText(this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
        }else if (textoSaldo.isEmpty()){
            Toast.makeText(this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
        } else if(textoNumCuenta.isEmpty()){
            Toast.makeText(this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
        }else {
            // Crear Bundle
            Bundle bundle = new Bundle();
            bundle.putString("nombreCliente", txtNombre.getText().toString());
            bundle.putString("saldoCliente", txtSaldo.getText().toString());
            bundle.putString("numeroCuenta", txtNumCuenta.getText().toString());
            bundle.putString("bancoCliente", txtBanco.getText().toString());

            // Crear intent
            Intent intent = new Intent(MainActivity.this, CuentaBancoActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad
            startActivity(intent);
        }

    }

    private void salir(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Banco Nacional SOMEX");
        confirmar.setMessage("Salir de la app");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        confirmar.show();
    }

}