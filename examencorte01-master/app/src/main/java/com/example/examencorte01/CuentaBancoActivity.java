package com.example.examencorte01;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class CuentaBancoActivity extends AppCompatActivity {
    private TextView lblNombre;
    private TextView lblSaldo;
    private EditText txtCantidad;
    private Button btnDeposito;
    private Button btnRetiro;
    private Button btnRegresar;
    private String nombreCliente;
    private String saldoCliente;
    private String numeroCuenta;
    private String bancoCliente;

    // Declarar objeto
    CuentaBanco cuenta = new CuentaBanco();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);
        iniciarComponentes();
        Bundle datos = getIntent().getExtras();
        if (datos != null) {
            nombreCliente = datos.getString("nombreCliente");
            saldoCliente = datos.getString("saldoCliente");
            numeroCuenta = datos.getString("numeroCuenta");
            bancoCliente = datos.getString("bancoCliente");
            if (nombreCliente != null && saldoCliente != null && numeroCuenta != null && bancoCliente != null) {
                lblNombre.setText(nombreCliente);
                lblSaldo.setText(saldoCliente);

            }
        }
        btnDeposito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metodoDeposito();
            }
        });
        btnRetiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metodoRetiro();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                metodoRegresar();
            }
        });
    }

    private void iniciarComponentes() {
        lblNombre = findViewById(R.id.lblNombre);
        lblSaldo = findViewById(R.id.lblSaldo);
        txtCantidad = findViewById(R.id.txtCantidad);
        btnDeposito = findViewById(R.id.btnDeposito);
        btnRetiro = findViewById(R.id.btnRetiro);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Inicializar objeto
        cuenta = new CuentaBanco();

    }

    private void metodoDeposito() {
        cuenta.setBanco(bancoCliente.toString());
        cuenta.setNumCuenta(Integer.parseInt(numeroCuenta.toString()));
        cuenta.setNombre(nombreCliente.toString());
        cuenta.setSaldo(Float.parseFloat(saldoCliente.toString()));

        if (txtCantidad.getText().toString().isEmpty()) {
            Toast.makeText(this, "Ingrese la cantidad", Toast.LENGTH_SHORT).show();
        } else {
            cuenta.depositar(Float.parseFloat(txtCantidad.getText().toString()));
            lblSaldo.setText(String.valueOf(cuenta.getSaldo()));
            saldoCliente = String.valueOf(cuenta.getSaldo());
        }
    }

    private void metodoRetiro() {
        if (txtCantidad.getText().toString().isEmpty()) {
            Toast.makeText(this, "Ingrese la cantidad", Toast.LENGTH_SHORT).show();
        } else {
            float cantidad = Float.parseFloat(txtCantidad.getText().toString());

            if (!cuenta.retirar(cantidad)) {
                Toast.makeText(this, "Fondos insuficientes", Toast.LENGTH_SHORT).show();
            } else {
                lblSaldo.setText(String.valueOf(cuenta.getSaldo()));
                saldoCliente = String.valueOf(cuenta.getSaldo());
            }
        }
    }


    private void metodoRegresar(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Banco Nacional SOMEX");
        confirmar.setMessage("Regresar a la pantalla inicial");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
            }
        });
        confirmar.show();
    }


}
