package com.example.examobrec_javaser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {
    private EditText  txtDescripcion, txtValorAuto, txtPorcentaje;
    private RadioGroup rdbMeses;
    private TextView txtFolio,txtNombre ,lblPagoMensual, lblEnganche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        iniciar();
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString(MainActivity.EXTRA_MESSAGE);
    generarFol();
        txtNombre.setText(usuario);

    }
    private void iniciar(){
        txtFolio = findViewById(R.id.lblFolio);
        txtNombre = findViewById(R.id.lblCliente);
        txtDescripcion= findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValorAuto);
        txtPorcentaje = findViewById(R.id.txtPorEng);
        rdbMeses = findViewById(R.id.rdbMeses);
        lblPagoMensual = findViewById(R.id.lblPagoM);
        lblEnganche = findViewById(R.id.lblEnganche);
        
    }
    public void generarFol(){
        double numeroAleatorio = Math.random();

        // Multiplica el número aleatorio por un rango para obtener un número en un rango específico
        int minimo = 1;
        int maximo = 1000;
        int rango = maximo - minimo + 1;
        int folio= (int)(numeroAleatorio * rango) + minimo;
        txtFolio.setText(String.valueOf(folio));

    }
    public void calcular(View v) {
        // Obtener los valores de entrada de los EditTexts

        RadioButton selectedRadioButton = findViewById(rdbMeses.getCheckedRadioButtonId());

        if (txtDescripcion.getText().toString().equals("") || txtValorAuto.getText().toString().equals("") || selectedRadioButton == null || txtPorcentaje.getText().toString().equals("") ){
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
        }
        else {
            int folio=Integer.parseInt(txtFolio.getText().toString());
            String descripcion = txtDescripcion.getText().toString();
            float valorAuto = Float.parseFloat(txtValorAuto.getText().toString());
            float porEnganche = Float.parseFloat(txtPorcentaje.getText().toString());
            int plazo = 0;
            String puestoText = selectedRadioButton.getText().toString();
            if(porEnganche<1 || porEnganche>100){
                Toast.makeText(this, "Porcentaje erroneo", Toast.LENGTH_SHORT).show();
            }
            else {
                if (puestoText.equalsIgnoreCase("12 meses")) {
                    plazo = 12;
                } else if (puestoText.equalsIgnoreCase("18 meses")) {
                    plazo = 18;
                } else if (puestoText.equalsIgnoreCase("24 meses")) {
                    plazo = 24;
                } else if (puestoText.equalsIgnoreCase("36 meses")) {
                    plazo = 36;
                }
                Cotizacion Coti = new Cotizacion(folio,descripcion, valorAuto, porEnganche, plazo);
                float enganche = Coti.calcularEnganche();
                float pagom = Coti.calcularPago();

                lblEnganche.setText(String.valueOf(enganche));
                lblPagoMensual.setText(String.valueOf(pagom));

            }
        }
    }

    public void limpiar(View v) {


        txtNombre = findViewById(R.id.lblCliente);
        txtDescripcion.setText("");
        txtValorAuto.setText("");
        txtPorcentaje.setText("");

        lblPagoMensual.setText("");
        lblEnganche.setText("");
    }
    public void regresar(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar a la vista pricipal?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
}