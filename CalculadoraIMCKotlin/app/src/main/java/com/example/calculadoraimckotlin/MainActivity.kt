package com.example.calculadoraimckotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private var editTextWeight: EditText? = null
    private var editTextHeight: EditText? = null
    private var buttonCalculate: Button? = null
    private var textViewResult: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        editTextWeight = findViewById<EditText>(R.id.editTextWeight)
        editTextHeight = findViewById<EditText>(R.id.editTextHeight)
        buttonCalculate = findViewById<Button>(R.id.buttonCalculate)
        textViewResult = findViewById<TextView>(R.id.textViewResult)
        buttonCalculate.setOnClickListener(View.OnClickListener { calculateBMI() })
    }

    fun Limpiar(v: View?) {
        editTextWeight!!.setText("")
        editTextHeight!!.setText("")
        textViewResult!!.text = ""
    }

    fun Cerrar(v: View?) {
        finish()
    }

    private fun calculateBMI() {
        val weightString = editTextWeight!!.text.toString()
        val heightString = editTextHeight!!.text.toString()
        if (!weightString.isEmpty() && !heightString.isEmpty()) {
            val weight = weightString.toFloat()
            val height = heightString.toFloat()
            val bmi = weight / (height * height)
            val result = "Tu IMC es: $bmi"
            textViewResult!!.text = result
        } else {
            textViewResult!!.text = "Por favor, introduce peso y altura."
        }
    }
}