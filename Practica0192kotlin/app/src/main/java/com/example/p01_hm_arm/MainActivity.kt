package com.example.p01_hm_arm

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    lateinit var nombreUser: EditText
    lateinit var Salud0: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun saludar(v: View?) {
        nombreUser = findViewById(R.id.txtNombre)
        Salud0 = findViewById(R.id.lbSaludo)

        
        if (nombreUser.getText().toString()=="") {
            Toast.makeText(this, "Falto capturar un nombre", Toast.LENGTH_SHORT).show()
        } else {
            Salud0.setText("Hola " + nombreUser.getText().toString())
        }
    }

    fun limpiar(v: View?) {
        if (nombreUser!!.text.toString()==""&& Salud0!!.text.toString()=="") {
            Toast.makeText(this, "No hay nada escrito", Toast.LENGTH_SHORT).show()
        } else {
            Salud0!!.text = ""
            nombreUser!!.setText("")
        }
    }

    fun Fin(v: View?) {
        finish()
    }
}