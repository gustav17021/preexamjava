package com.example.listview92;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MiAdaptador extends RecyclerView.Adapter<MiAdaptador.ViewHolder> implements View.OnClickListener, Filterable {

    protected ArrayList<Alumno> listaAlumnos;
    private View.OnClickListener listener;
    private Context context;
    private LayoutInflater inflater;
    private List<Alumno> originalList;
    private List<Alumno> filteredList;

    public MiAdaptador(ArrayList<Alumno> listaAlumnos, Context context, List<Alumno> items) {
        this.listaAlumnos = listaAlumnos;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.originalList = items;
        this.filteredList = new ArrayList<>(items);
    }

    @NonNull
    @Override
    public MiAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.alumnos_items, null);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull MiAdaptador.ViewHolder holder, int position) {
        Alumno alumno = filteredList.get(position);
        holder.txtMatricula.setText(alumno.getMatricula());
        holder.txtNombre.setText(alumno.getNombre());
        holder.idImagen.setImageResource(alumno.getImg());
    }


    public void setOnClickListener(View.OnClickListener listener) { this.listener = listener; }

    @Override
    public void onClick(View v) { if (listener != null) listener.onClick(v); }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LayoutInflater inflater;
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            txtNombre = (TextView) itemView.findViewById((R.id.txtAlumnoNombre));
            txtMatricula = (TextView) itemView.findViewById((R.id.txtMatricula));
            txtCarrera = (TextView) itemView.findViewById((R.id.txtCarrera));
            idImagen = (ImageView) itemView.findViewById((R.id.foto));
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<Alumno> filteredItems = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredItems.addAll(originalList);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();

                    for (Alumno item : originalList) {
                        if (item.getNombre().toLowerCase().contains(filterPattern) ||
                                item.getMatricula().toLowerCase().contains(filterPattern)) {
                            filteredItems.add(item);
                        }
                    }
                }

                results.values = filteredItems;
                results.count = filteredItems.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList.clear();
                filteredList.addAll((List<Alumno>) results.values);
                notifyDataSetChanged();
            }

        };
    }
    public Alumno getItem(int position) {
        return filteredList.get(position);
    }
}
