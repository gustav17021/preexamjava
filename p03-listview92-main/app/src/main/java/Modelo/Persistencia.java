package Modelo;

import com.example.listview92.AlumnoItem;

public interface Persistencia {

    public void openDataBase();
    public void closeDataBase();
    public long insertAlumno(AlumnoItem alumno);
    public long updateAlumno(AlumnoItem alumno);
    public void deleteAlumno(int id);


}
