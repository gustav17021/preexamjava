package com.example.preex_kotlin_arm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class ReciboActivity : AppCompatActivity() {
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtHorasTrabNormales: EditText
    private lateinit var txtHorasTrabExtra: EditText
    private lateinit var rdbPuesto: RadioGroup
    private lateinit var lblSubtotalTotal: TextView
    private lateinit var lblImpuestoTotal: TextView
    private lateinit var lblTotalTotal: TextView
    private lateinit var lblUsuario: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo)
        iniciar()
        val datos = intent.extras
        val usuario = datos?.getString(MainActivity.EXTRA_MESSAGE)

        lblUsuario.text = usuario
    }

    private fun iniciar() {
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasTrabNormales = findViewById(R.id.txtHorasTrabNormales)
        txtHorasTrabExtra = findViewById(R.id.txtHorasTrabExtra)
        rdbPuesto = findViewById(R.id.rdbPuesto)
        lblSubtotalTotal = findViewById(R.id.lblSubtotalTotal)
        lblImpuestoTotal = findViewById(R.id.lblImpuestoTotal)
        lblTotalTotal = findViewById(R.id.lblTotalTotal)
        lblUsuario = findViewById(R.id.lblUsuario)
    }

    fun calcular(v: View) {
        // Obtener los valores de entrada de los EditTexts
        val selectedRadioButton = findViewById<RadioButton>(rdbPuesto.checkedRadioButtonId)

        if (txtNumRecibo.text.toString() == "" || txtNombre.text.toString() == "" || selectedRadioButton == null || txtHorasTrabNormales.text.toString() == "" || txtHorasTrabExtra.text.toString() == "") {
            Toast.makeText(this, "Datos erroneos", Toast.LENGTH_SHORT).show()
        } else {
            val numRecibo = txtNumRecibo.text.toString().toInt()
            val nombre = txtNombre.text.toString()
            val horasNormales = txtHorasTrabNormales.text.toString().toFloat()
            val horasExtra = txtHorasTrabExtra.text.toString().toFloat()
            var puesto = 0
            val puestoText = selectedRadioButton.text.toString()

            when {
                puestoText.equals("Auxiliar", ignoreCase = true) -> puesto = 1
                puestoText.equals("Albañil", ignoreCase = true) -> puesto = 2
                puestoText.equals("Ing Obra", ignoreCase = true) -> puesto = 3
            }

            val reciboNomina = ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto, 16f)
            val subtotal = reciboNomina.calcularSubtotal()
            val impuesto = reciboNomina.calcularImpuesto()
            val total = reciboNomina.calcularTotal()

            lblSubtotalTotal.text = subtotal.toString()
            lblImpuestoTotal.text = impuesto.toString()
            lblTotalTotal.text = total.toString()
        }
    }

    fun limpiar(v: View) {
        txtNumRecibo.text.clear()
        txtNombre.text.clear()
        txtHorasTrabNormales.text.clear()
        txtHorasTrabExtra.text.clear()
        rdbPuesto.clearCheck()
        lblSubtotalTotal.text = ""
        lblImpuestoTotal.text = ""
        lblTotalTotal.text = ""
    }

    fun regresar(v: View) {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Regresar a el login?")
        confirmar.setPositiveButton("Confirmar") { dialog, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, which -> dialog.dismiss() }
        confirmar.show()
    }
}