package com.example.preex_kotlin_arm

class ReciboNomina(
    private var numR: Int,
    private var nombre: String,
    private var horasTrabNormal: Float,
    private var horasTrabExtras: Float,
    private var puesto: Int,
    private var impuestoPorc: Float
) {
    // Métodos para cálculos
    fun reciboNominal(): Float {
        val pagoBase = 200f
        return when (puesto) {
            1 -> pagoBase * 1.2f
            2 -> pagoBase * 1.5f
            3 -> pagoBase * 2.0f
            else -> pagoBase
        }
    }

    fun calcularSubtotal(): Float {
        val pagoBase = reciboNominal()
        val pagoHoraExtra = pagoBase * 2
        return (pagoBase * horasTrabNormal) + (pagoHoraExtra * horasTrabExtras)
    }

    fun calcularImpuesto(): Float {
        val subtotal = calcularSubtotal()
        return subtotal * (impuestoPorc / 100)
    }

    fun calcularTotal(): Float {
        val subtotal = calcularSubtotal()
        val impuesto = calcularImpuesto()
        return subtotal - impuesto
    }

    // Getters and Setters
    fun getNumR(): Int {
        return numR
    }

    fun setNumR(numR: Int) {
        this.numR = numR
    }

    fun getNombre(): String {
        return nombre
    }

    fun setNombre(nombre: String) {
        this.nombre = nombre
    }

    fun getHorasTrabNormal(): Float {
        return horasTrabNormal
    }

    fun setHorasTrabNormal(horasTrabNormal: Float) {
        this.horasTrabNormal = horasTrabNormal
    }

    fun getHorasTrabExtras(): Float {
        return horasTrabExtras
    }

    fun setHorasTrabExtras(horasTrabExtras: Float) {
        this.horasTrabExtras = horasTrabExtras
    }

    fun getPuesto(): Int {
        return puesto
    }

    fun setPuesto(puesto: Int) {
        this.puesto = puesto
    }

    fun getImpuestoPorc(): Float {
        return impuestoPorc
    }

    fun setImpuestoPorc(impuestoPorc: Float) {
        this.impuestoPorc = impuestoPorc
    }
}