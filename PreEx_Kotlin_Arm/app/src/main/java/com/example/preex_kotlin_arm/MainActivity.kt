package com.example.preex_kotlin_arm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var txtUser: EditText

    companion object {
        const val EXTRA_MESSAGE = "com.example.MainActivity.MESSAGE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtUser = findViewById(R.id.txtUsuario)
    }

    fun Ingresar(v: View) {
        val strNombre = getString(R.string.nombre)

        if (strNombre == txtUser.text.toString()) {
            val intent = Intent(this, ReciboActivity::class.java)
            val message = txtUser.text.toString()
            intent.putExtra(EXTRA_MESSAGE, message)
            startActivity(intent)
            txtUser.text.clear()
        } else {
            Toast.makeText(applicationContext, "El usuario no existe", Toast.LENGTH_SHORT).show()
        }
    }

    fun Salir(v: View) {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo Nómina")
        confirmar.setMessage("¿Quieres salir de tu app?")
        confirmar.setPositiveButton("Confirmar") { dialog, which ->
            finish()
        }
        confirmar.setNegativeButton("Cancelar") { dialog, which ->
            dialog.dismiss()
        }
        confirmar.show()
    }
}