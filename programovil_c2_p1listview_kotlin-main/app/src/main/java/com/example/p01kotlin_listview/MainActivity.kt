package com.example.p01kotlin_listview

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var spnPaises: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        spnPaises = findViewById(R.id.spnPaises)
        val adaptador = ArrayAdapter(
            this@MainActivity,
            android.R.layout.simple_expandable_list_item_1,
            resources.getStringArray(R.array.paises)
        )
        spnPaises.adapter = adaptador
        spnPaises.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(
                this@MainActivity,
                "Seleccionó el país " + adapterView.getItemAtPosition(i).toString(),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
