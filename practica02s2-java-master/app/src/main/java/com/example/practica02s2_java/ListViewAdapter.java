package com.example.practica02s2_java;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListViewAdapter extends ArrayAdapter<ItemData> {
    private Activity context;
    private int layoutResourceId;
    private ArrayList<ItemData> data;

    public ListViewAdapter(Activity context, int layoutResourceId, ArrayList<ItemData> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.imgCategoria = row.findViewById(R.id.imgCategoria);
            holder.lblCategorias = row.findViewById(R.id.lblCategorias);
            holder.lblDescripcion = row.findViewById(R.id.lblDescripcion);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ItemData item = data.get(position);

        holder.imgCategoria.setImageResource(item.getImageId());
        holder.lblCategorias.setText(item.getTextCategoria());
        holder.lblDescripcion.setText(item.getTextDescripcion());

        return row;
    }

    static class ViewHolder {
        ImageView imgCategoria;
        TextView lblCategorias;
        TextView lblDescripcion;
    }
}