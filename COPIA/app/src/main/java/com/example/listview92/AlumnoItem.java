package com.example.listview92;

public class AlumnoItem {
    private String textNombre;
    private String textMatricula;
    private Integer imageId;

    public AlumnoItem(String nombre, String matricula, Integer imageId){
        this.textNombre = nombre;
        this.textMatricula = matricula;
        this.imageId = imageId;
    }

    public String getTextNombre() { return textNombre; }
    public void setTextNombre(String textNombre) { this.textNombre = textNombre; }
    public String getTextMatricula() { return textMatricula; }
    public void setTextMatricula(String textMatricula) { this.textMatricula = textMatricula; }
    public Integer getImageId() { return imageId; }
    public void setImageId(Integer imageId) { this.imageId = imageId; }
}
