package com.example.listview92;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
public class AdapterAlumno extends ArrayAdapter<AlumnoItem> {
    int groupId;
    Activity Context;
    ArrayList<AlumnoItem> list;
    LayoutInflater inflater;

    public AdapterAlumno(Activity Context, int groupId, int id, ArrayList<AlumnoItem> list) {
        super(Context, id, list);
        this.list = list;
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    public View getView(int posicion, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupId, parent, false);
        ImageView imagenAlumno = (ImageView) itemView.findViewById(R.id.imgAlumno);
        imagenAlumno.setImageResource(list.get(posicion).getImageId());
        TextView textNombre = (TextView) itemView.findViewById(R.id.lblNombre);
        textNombre.setText(list.get(posicion).getTextNombre());
        TextView textMatricula = (TextView) itemView.findViewById(R.id.lblMatricula);
        textMatricula.setText(list.get(posicion).getTextMatricula());
        return itemView;
    }

    public View getDropDownView(int posicion, View convertView, ViewGroup parent) {
        return getView(posicion, convertView, parent);
    }
}