package com.example.listview92;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<AlumnoItem> list = new ArrayList<>();
        list.add(new AlumnoItem(getString(R.string.nombre344), getString(R.string.matricula344), R.drawable.ic_2019030344));
        list.add(new AlumnoItem(getString(R.string.nombre174), getString(R.string.matricula174), R.drawable.ic_2020030174));
        list.add(new AlumnoItem(getString(R.string.nombre176), getString(R.string.matricula176), R.drawable.ic_2020030176));
        list.add(new AlumnoItem(getString(R.string.nombre181), getString(R.string.matricula181), R.drawable.ic_2020030181));
        list.add(new AlumnoItem(getString(R.string.nombre184), getString(R.string.matricula184), R.drawable.ic_2020030184));
        list.add(new AlumnoItem(getString(R.string.nombre189), getString(R.string.matricula189), R.drawable.ic_2020030189));
        list.add(new AlumnoItem(getString(R.string.nombre199), getString(R.string.matricula199), R.drawable.ic_2020030199));
        list.add(new AlumnoItem(getString(R.string.nombre212), getString(R.string.matricula212), R.drawable.ic_2020030212));
        list.add(new AlumnoItem(getString(R.string.nombre241), getString(R.string.matricula241), R.drawable.ic_2020030241));
        list.add(new AlumnoItem(getString(R.string.nombre243), getString(R.string.matricula243), R.drawable.ic_2020030243));
        list.add(new AlumnoItem(getString(R.string.nombre249), getString(R.string.matricula249), R.drawable.ic_2020030249));
        list.add(new AlumnoItem(getString(R.string.nombre264), getString(R.string.matricula264), R.drawable.ic_2020030264));
        list.add(new AlumnoItem(getString(R.string.nombre268), getString(R.string.matricula268), R.drawable.ic_2020030268));
        list.add(new AlumnoItem(getString(R.string.nombre292), getString(R.string.matricula292), R.drawable.ic_2020030292));
        list.add(new AlumnoItem(getString(R.string.nombre304), getString(R.string.matricula304), R.drawable.ic_2020030304));
        list.add(new AlumnoItem(getString(R.string.nombre306), getString(R.string.matricula306), R.drawable.ic_2020030306));
        list.add(new AlumnoItem(getString(R.string.nombre313), getString(R.string.matricula313), R.drawable.ic_2020030313));
        list.add(new AlumnoItem(getString(R.string.nombre315), getString(R.string.matricula315), R.drawable.ic_2020030315));
        list.add(new AlumnoItem(getString(R.string.nombre322), getString(R.string.matricula322), R.drawable.ic_2020030322));
        list.add(new AlumnoItem(getString(R.string.nombre325), getString(R.string.matricula325), R.drawable.ic_2020030325));
        list.add(new AlumnoItem(getString(R.string.nombre327), getString(R.string.matricula327), R.drawable.ic_2020030327));
        list.add(new AlumnoItem(getString(R.string.nombre329), getString(R.string.matricula329), R.drawable.ic_2020030329));
        list.add(new AlumnoItem(getString(R.string.nombre332), getString(R.string.matricula332), R.drawable.ic_2020030332));
        list.add(new AlumnoItem(getString(R.string.nombre333), getString(R.string.matricula333), R.drawable.ic_2020030333));
        list.add(new AlumnoItem(getString(R.string.nombre389), getString(R.string.matricula389), R.drawable.ic_2020030389));
        list.add(new AlumnoItem(getString(R.string.nombre766), getString(R.string.matricula766), R.drawable.ic_2020030766));
        list.add(new AlumnoItem(getString(R.string.nombre771), getString(R.string.matricula771), R.drawable.ic_2020030771));
        list.add(new AlumnoItem(getString(R.string.nombre777), getString(R.string.matricula777), R.drawable.ic_2020030777));
        list.add(new AlumnoItem(getString(R.string.nombre799), getString(R.string.matricula799), R.drawable.ic_2020030799));
        list.add(new AlumnoItem(getString(R.string.nombre808), getString(R.string.matricula808), R.drawable.ic_2020030808));
        list.add(new AlumnoItem(getString(R.string.nombre819), getString(R.string.matricula819), R.drawable.ic_2020030819));
        list.add(new AlumnoItem(getString(R.string.nombre865), getString(R.string.matricula865), R.drawable.ic_2020030865));
        lv = (ListView) findViewById(R.id.listView1);

        ListAdapter AlumnoAdapter = new AdapterAlumno(this, R.layout.activity_adapter_alumno, R.id.lblNombre, list);
        lv.setAdapter(AlumnoAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                Toast.makeText(adapterView.getContext(), getString(R.string.AlumnoSeleccionado).toString() + " " + ((AlumnoItem) adapterView.getItemAtPosition(i)).getTextNombre(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}