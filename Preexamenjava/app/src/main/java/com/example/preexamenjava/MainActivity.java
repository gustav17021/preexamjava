package com.example.preexamenjava;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtUser;


    public static final String EXTRA_MESSAGE = "com.example.MainActivity.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUser = findViewById(R.id.txtUsuario);
    }
    public void Ingresar(View v) {
        String strNombre;

        strNombre = getString(R.string.nombre);

        if (strNombre.equals(txtUser.getText().toString())) {
            Intent intent = new Intent(this, ReciboActivity.class);
            String message = txtUser.getText().toString();
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
            txtUser.setText("");

        } else {
            Toast.makeText(getApplicationContext(), "El usuario no esta registrado", Toast.LENGTH_SHORT).show();
        }
    }

    public void Salir(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Recibo Nómina");
        confirmar.setMessage("Quieres salir de la app?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
}
