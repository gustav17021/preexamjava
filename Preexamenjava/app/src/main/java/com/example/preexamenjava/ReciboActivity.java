package com.example.preexamenjava;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class ReciboActivity extends AppCompatActivity {
    private EditText txtNumRecibo, txtNombre, txtHorasTrabNormales, txtHorasTrabExtra;
    private RadioGroup rdbPuesto;
    private TextView lblSubtotalTotal, lblImpuestoTotal, lblTotalTotal, lblUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);
        iniciar();
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString(MainActivity.EXTRA_MESSAGE);

        lblUsuario.setText(usuario);

    }
    private void iniciar(){
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasTrabNormales = findViewById(R.id.txtHorasTrabNormales);
        txtHorasTrabExtra = findViewById(R.id.txtHorasTrabExtra);
        rdbPuesto = findViewById(R.id.rdbPuesto);
        lblSubtotalTotal = findViewById(R.id.lblSubtotalTotal);
        lblImpuestoTotal = findViewById(R.id.lblImpuestoTotal);
        lblTotalTotal = findViewById(R.id.lblTotalTotal);
        lblUsuario=findViewById(R.id.lblUsuario);
    }
    public void calcular(View v) {
        RadioButton selectedRadioButton = findViewById(rdbPuesto.getCheckedRadioButtonId());
        if (txtNumRecibo.getText().toString().equals("") || txtNombre.getText().toString().equals("") || selectedRadioButton == null || txtHorasTrabNormales.getText().toString().equals("") || txtHorasTrabExtra.getText().toString().equals("")){
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
        }
        else {
            int numRecibo = Integer.parseInt(txtNumRecibo.getText().toString());
            String nombre = txtNombre.getText().toString();
            float horasNormales = Float.parseFloat(txtHorasTrabNormales.getText().toString());
            float horasExtra = Float.parseFloat(txtHorasTrabExtra.getText().toString());
            int puesto = 0;
            String puestoText = selectedRadioButton.getText().toString();

            if (puestoText.equalsIgnoreCase("Auxiliar")) {
                puesto = 1;
            } else if (puestoText.equalsIgnoreCase("Albañil")) {
                puesto = 2;
            } else if (puestoText.equalsIgnoreCase("Ing Obra")) {
                puesto = 3;
            }
            ReciboNomina reciboNomina = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto, 16f);
            float subtotal = reciboNomina.calcularSubtotal();
            float impuesto = reciboNomina.calcularImpuesto();
            float total = reciboNomina.calcularTotal();
            lblSubtotalTotal.setText(String.valueOf(subtotal));
            lblImpuestoTotal.setText(String.valueOf(impuesto));
            lblTotalTotal.setText(String.valueOf(total));
        }
    }

    public void limpiar(View v) {

        txtNumRecibo.setText("");
        txtNombre.setText("");
        txtHorasTrabNormales.setText("");
        txtHorasTrabExtra.setText("");
        rdbPuesto.clearCheck();
        lblSubtotalTotal.setText("");
        lblImpuestoTotal.setText("");
        lblTotalTotal.setText("");
    }
    public void regresar(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar al login?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
}//Se realizaron pruebas pruebas y se verifico las validaciones.