package com.example.preexamenjava;

public class ReciboNomina {
    private int numR;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public ReciboNomina(int numR, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numR = numR;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }



    // Métodos para cálculos

    public float reciboNominal() {
        float pagoBase = 200f;
        switch (puesto) {
            case 1:
                return pagoBase * 1.2f;
            case 2:
                return pagoBase * 1.5f;
            case 3:
                return pagoBase * 2.0f;
            default:
                return pagoBase;
        }
    }

    public float calcularSubtotal() {
        float pagoBase = reciboNominal();
        float pagoHoraExtra = pagoBase * 2;
        return (pagoBase * horasTrabNormal) + (pagoHoraExtra * horasTrabExtras);
    }

    public float calcularImpuesto() {
        float subtotal = calcularSubtotal();
        return subtotal * (impuestoPorc / 100);
    }

    public float calcularTotal() {
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        return subtotal - impuesto;
    }
    // Getters y Setters
    public int getNumR() {
        return numR;
    }

    public void setNumR(int numR) {
        this.numR = numR;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }
}//Se realizaron pruebas pruebas y se verifico las validaciones.
