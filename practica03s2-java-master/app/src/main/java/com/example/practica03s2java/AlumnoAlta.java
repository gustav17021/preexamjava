package com.example.practica03s2java;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.Manifest;

import java.util.ArrayList;

import Modelo.AlumnosDb;


public class AlumnoAlta extends AppCompatActivity {

    private Button btnGuardar, btnRegresar,btnEliminar;
    private Alumno alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private TextView lblImagen;
    private String carrera;
    private int posicion;
    private AlumnosDb alumnosDb;
    private String imagenSeleccionada;
    private static final int REQUEST_SELECT_IMAGE = 1;
    private static final int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alumno_alta);
        btnGuardar = (Button) findViewById(R.id.btnSalir);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtGrado = (EditText) findViewById(R.id.txtGrado);
        imgAlumno = (ImageView) findViewById(R.id.imgAlumno);
        lblImagen= (TextView) findViewById(R.id.lblFoto);

        Bundle bundle = getIntent().getExtras();
        alumno = (Alumno) bundle.getSerializable("alumno");
        posicion = bundle.getInt("posicion", posicion);

        if(posicion >= 0 && alumno!=null){
            txtMatricula.setText(alumno.getTextMatricula());
            txtNombre.setText(alumno.getTextNombre());
            txtGrado.setText(alumno.getTextCarrera());
            //imgAlumno.setImageResource(alumno.getImageId());

            //if (alumno.getImageId() != null) {
                // Cargar la imagen utilizando Picasso y mostrarla en el ImageView
                //Picasso.get().load(new File(alumno.getImageId())).into(imgAlumno);
            //}
            if (alumno.getImageId() != null) {
                Uri imageUri = Uri.parse(alumno.getImageId());
                imgAlumno.setImageURI(imageUri);
            } else {
                // Manejar la situación cuando no hay una URI válida
                imgAlumno.setImageResource(R.drawable.us01);
            }

            //imgAlumno.setImageURI(Uri.parse(alumno.getImageId()));
            //lblImagen.setText(alumno.getImageId().toString());

        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(posicion >= 0 && alumno!=null){
                    // Modificar Alumno
                    alumno.setTextMatricula(txtMatricula.getText().toString());
                    alumno.setTextNombre(txtNombre.getText().toString());
                    alumno.setTextCarrera(txtGrado.getText().toString());

                    if (imagenSeleccionada != null) {
                        alumno.setImageId(imagenSeleccionada);
                        lblImagen.setText(imagenSeleccionada.toString());
                    }

                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setImageId(alumno.getImageId());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setTextMatricula(alumno.getTextMatricula());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setTextNombre(alumno.getTextNombre());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setTextCarrera(alumno.getTextCarrera());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setId(alumno.getId());

                    alumnosDb = new AlumnosDb(getApplicationContext());
                    alumnosDb.updateAlumno(alumno);

                    Toast.makeText(getApplicationContext(), " se modificó con éxito", Toast.LENGTH_SHORT).show();
                    // Actualizar el alumno en la lista listaAlumnos del adaptador
                    MiAdaptador adaptador = ((Aplicacion) getApplication()).getAdaptador();
                    adaptador.actualizarAlumno(posicion, alumno);
                } else {
                    // Agregar un nuevo alumno
                    String matricula = txtMatricula.getText().toString();
                    String nombre = txtNombre.getText().toString();
                    String grado = txtGrado.getText().toString();

                    if (matricula.isEmpty() || nombre.isEmpty() || grado.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Faltó capturar datos", Toast.LENGTH_SHORT).show();
                        txtMatricula.requestFocus();
                    } else {
                        alumno = new Alumno();
                        alumno.setTextMatricula(matricula);
                        alumno.setTextNombre(nombre);
                        alumno.setTextCarrera(grado);

                        if (imagenSeleccionada != null) {
                            alumno.setImageId(imagenSeleccionada);
                            lblImagen.setText(imagenSeleccionada.toString());
                        }

                        alumnosDb = new AlumnosDb(getApplicationContext());
                        long idAlumno=alumnosDb.insertAlumno(alumno);

                        alumno.setId((int) idAlumno);

                        ((Aplicacion) getApplication()).getAlumnos().add(alumno);
                        ((Aplicacion) getApplication()).getAdaptador().notifyDataSetChanged(); // Notificar al adaptador del cambio en los datos

                        setResult(Activity.RESULT_OK);
                        finish();

                        Toast.makeText(getApplicationContext(), "Se guardó con éxito", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if (posicion >= 0 && alumno != null) {

                    AlertDialog.Builder confirmar = new AlertDialog.Builder(AlumnoAlta.this);
                    confirmar.setTitle("Eliminar");
                    confirmar.setMessage("¿Desea eliminar a este alumno?");
                    confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Eliminar Alumno
                            alumnosDb = new AlumnosDb(getApplicationContext());
                            alumnosDb.deleteAlumnos(alumno.getId());

                            ((Aplicacion) getApplication()).getAlumnos().remove(posicion);
                            ((Aplicacion) getApplication()).getAdaptador().notifyDataSetChanged();

                            // Eliminar el alumno de la lista listaAlumnos del adaptador
                            //MiAdaptador adaptador = ((Aplicacion) getApplication()).getAdaptador();
                            //ArrayList<Alumno> listaAlumnos = adaptador.getListaAlumnos();
                            //listaAlumnos.remove(alumno);
                            //adaptador.notifyDataSetChanged();

                            ((Aplicacion) getApplication()).getAdaptador().actualizarListaCompleta(((Aplicacion) getApplication()).getAlumnos());

                            setResult(Activity.RESULT_OK);
                            finish();

                            Toast.makeText(getApplicationContext(), "Se eliminó el alumno", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                    confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Cancelar, no se realiza ninguna acción
                        }
                    });
                    confirmar.show();
                }
            }
        });

        imgAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Se llama a la función openGallery;
                openGallery();
            }
        });
    }

    private void openGallery(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSION_READ_EXTERNAL_STORAGE);
            }
        } else {
            selectImage();
        }
    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_SELECT_IMAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage();
            } else {
                Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_SELECT_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri selectedImageUri = data.getData();
            imagenSeleccionada = selectedImageUri.toString();

            imgAlumno.setImageURI(selectedImageUri);
        }
    }


}

