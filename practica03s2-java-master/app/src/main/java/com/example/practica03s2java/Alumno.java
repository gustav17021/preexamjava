package com.example.practica03s2java;

import java.io.Serializable;
import java.util.ArrayList;

public class Alumno implements Serializable {
    private int id;
    private String Carrera;
    private String Matricula;
    private String Nombre;
    private String imageId;

    public Alumno() {

    }

    public Alumno(String matricula, String nombre, String imageId, String carrera){
        this.Matricula = matricula;
        this.Nombre = nombre;
        this.imageId =imageId;
        this.Carrera=carrera;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getTextCarrera() { return Carrera; }
    public void setTextCarrera(String Carrera) { this.Carrera = Carrera; }
    public String getTextMatricula() { return Matricula; }
    public void setTextMatricula(String text2) { this.Matricula = text2; }
    public String getTextNombre(){ return Nombre; }
    public void setTextNombre(String text){
        this.Nombre = text;
    }
    public String getImageId(){
        return imageId;
    }
    public void setImageId(String imageId){
        this.imageId = imageId;
    }

    public static ArrayList<Alumno> llenarAlumnos(){
        ArrayList<Alumno> alumnos=new ArrayList<>();

        /* alumnos.add(new Alumno("2019030344","MORUA ZAMUDIO ESTEFANO",R.drawable.s2019030344,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030174","CARRANZA JAUREGUI CARLOS ALBERTO",R.drawable.s2020030174,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030176","CASTRO LOPEZ MARCO ANTONIO ALARID",R.drawable.s2020030176,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030181","DURAN VALDEZ JOSHUA DANIEL",R.drawable.s2020030181,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030184","GALINDO HERNANDEZ ERNESTO DAVID",R.drawable.s2020030184,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030189","CONTRERAS CEPEDA MAXIMILIANO",R.drawable.s2020030189,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030199","GOMEZ RUELAS IVÁN ENRIQUE",R.drawable.s2020030199,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030212","CRUZ QUINTERO JESUS EDUARDO",R.drawable.s2020030212,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030241","VELARDE OVALLE DAVID ANTONIO",R.drawable.s2020030241,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030243","LAMAS ARMENTA GUSTAVO ADOLFO",R.drawable.s2020030243,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030249","RIVAS LUGO JUAN CARLOS",R.drawable.s2020030249,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030264","SALAS MENDOZA ALEJO",R.drawable.s2020030264,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030268","SERRANO TORRES CARLOS JAIR",R.drawable.s2020030268,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030292","TIRADO ROMERO JESUS TADEO",R.drawable.s2020030292,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030304","CARRILLO GARCIA JAIR",R.drawable.s2020030304,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030306","ARIAS ZATARAIN DIEGO",R.drawable.s2020030306,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030313","VALDEZ MARTINEZ PAOLA EMIRET",R.drawable.s2020030313,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030315","IBARRA FLORES SALMA YARETH",R.drawable.s2020030315,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030322","LIZARRAGA MALDONADO JUAN ANTONIO",R.drawable.s2020030322,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030325","VIERA ROMERO ANGEL ZINEDINE ANASTACIO",R.drawable.s2020030325,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030327","TEJEDA PEINADO BLAS ALBERTO",R.drawable.s2020030327,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030329","VIERA ROMERO ANGEL RONALDO ANASTACIO",R.drawable.s2020030329,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030332","ELIZALDE VARGAS XIOMARA YAMILETH",R.drawable.s2020030332,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030333","SALCIDO SARABIA JESUS ANTONIO",R.drawable.s2020030333,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030389","RODRIGUEZ SANCHEZ YENNIFER CAROLINA",R.drawable.s2020030389,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030766","FLORES PRADO MANUEL ALEXIS",R.drawable.s2020030766,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030771","AGUIRRE TOSTADO VICTOR MOISES",R.drawable.s2020030771,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030777","DOMINGUEZ SARABIA HALACH UINIC",R.drawable.s2020030777,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030799","MACIEL NUÑEZ ENZO ALEJANDRO",R.drawable.s2020030799,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030808","BARRON VARGAS JOSE ALBERTO",R.drawable.s2020030808,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030819","MARTIN IBARRA GIANCARLO",R.drawable.s2020030819,"Ing. Tec. Información"));
        alumnos.add(new Alumno("2020030865","SANCHEZ OCEGUEDA LUIS ANGEL",R.drawable.s2020030865,"Ing. Tec. Información"));*/
        return alumnos;
    }

}
