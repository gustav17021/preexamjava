package com.example.practica03s2java;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MiAdaptador extends RecyclerView.Adapter<MiAdaptador.ViewHolder> implements View.OnClickListener, Filterable {
    protected ArrayList<Alumno> listaAlumnos;
    private View.OnClickListener listener;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Alumno> listaAlumnosFull; // Nueva variable para SearchView


    public MiAdaptador(ArrayList<Alumno> listaAlumnos, Context context) {
        this.listaAlumnos=listaAlumnos;
        this.context=context;
        this.inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listaAlumnosFull = new ArrayList<>(listaAlumnos); // Asignar la lista completa
    }

    public void filtrar(String query) {
        listaAlumnos.clear();
        if (query.isEmpty()) {
            listaAlumnos.addAll(listaAlumnosFull);
        } else {
            query = query.toLowerCase();
            for (Alumno alumno : listaAlumnosFull) {
                if (alumno.getTextNombre().toLowerCase().contains(query)
                        || alumno.getTextCarrera().toLowerCase().contains(query)
                        || alumno.getTextMatricula().toLowerCase().contains(query)) {
                    listaAlumnos.add(alumno);
                }
            }
        }
        notifyDataSetChanged();
    }


    @NonNull
    @NotNull
    @Override
    public MiAdaptador.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.alumnos_items, parent, false); // Cambiar esta línea
        view.setOnClickListener(this); // Escucha el evento click
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MiAdaptador.ViewHolder holder, int position) {
        Alumno alumno=listaAlumnos.get(position);
        holder.txtMatricula.setText(alumno.getTextMatricula());
        holder.txtNombre.setText(alumno.getTextNombre());
        holder.txtCarrera.setText(alumno.getTextCarrera());
        //holder.idImagen.setImageResource(alumno.getImageId());

        if (alumno.getImageId() != null && !alumno.getImageId().isEmpty()) {
            // Cargar la imagen utilizando la URI y mostrarla en el ImageView
            Log.d("MiAdaptador", "URI de la imagen: " + alumno.getImageId()); // Mensaje de depuración
            Uri imageUri = Uri.parse(alumno.getImageId());
            holder.idImagen.setImageURI(imageUri);
        } else {
            Log.d("MiAdaptador", "No hay imagen para mostrar"); // Mensaje de depuración
            // Si no hay imagen, puedes mostrar una imagen por defecto o dejarla vacía
            holder.idImagen.setImageResource(R.drawable.us01);
        }
    }

    @Override
    public int getItemCount() { return listaAlumnos != null ? listaAlumnos.size() : 0; }

    public void setOnClickListener(View.OnClickListener listener){ this.listener=listener;}

    @Override
    public void onClick(View v) { if(listener != null) listener.onClick(v); }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Alumno> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(listaAlumnosFull);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for (Alumno alumno : listaAlumnosFull) {
                        if (alumno.getTextNombre().toLowerCase().contains(filterPattern)
                                || alumno.getTextCarrera().toLowerCase().contains(filterPattern)
                                || alumno.getTextMatricula().toLowerCase().contains(filterPattern)) {
                            filteredList.add(alumno);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listaAlumnos.clear();
                listaAlumnos.addAll((ArrayList<Alumno>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LayoutInflater inflater;
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;
        public ViewHolder(@NonNull @NotNull View itemView){
            super(itemView);
            txtNombre=(TextView) itemView.findViewById(R.id.txtAlumnoNombre);
            txtMatricula=(TextView) itemView.findViewById(R.id.txtMatricula);
            txtCarrera=(TextView) itemView.findViewById(R.id.txtCarrera);
            idImagen=(ImageView) itemView.findViewById(R.id.foto);
        }
    }

    public void actualizarAlumno(int posicion, Alumno alumno) {
        listaAlumnos.set(posicion, alumno);
        notifyDataSetChanged();
    }

    public ArrayList<Alumno> getListaAlumnos() {
        return listaAlumnos;
    }

    public void actualizarListaCompleta(ArrayList<Alumno> listaCompleta) {
        listaAlumnosFull.clear();
        listaAlumnosFull.addAll(listaCompleta);
        notifyDataSetChanged();
    }

}
