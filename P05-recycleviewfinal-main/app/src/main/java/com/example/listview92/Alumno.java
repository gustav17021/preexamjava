package com.example.listview92;

import java.io.Serializable;
import java.util.ArrayList;

public class Alumno implements Serializable {
    private int id;
    private String carrera;
    private String nombre;
    private Integer img;
    private String matricula;
    private String imagenUrl;

    public Alumno(){
        this.carrera = "";
        this.nombre = "";
        this.img = 0;
        this.matricula = "";
        this.imagenUrl = "";
    }

    public Alumno(String carrera, String nombre, Integer img, String matricula, String imagenUrl){
        this.carrera = carrera;
        this.nombre = nombre;
        this.img = img;
        this.matricula = matricula;
        this.imagenUrl = imagenUrl;
    }
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getCarrera() { return carrera; }
    public void setCarrera(String carrera) { this.carrera = carrera; }
    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    public int getImg() { return img; }
    public void setImg(Integer img) { this.img = img; }
    public String getMatricula() { return matricula; }
    public void setMatricula(String matricula) { this.matricula = matricula; }
    public String getImagenUrl() {
        return imagenUrl;
    }
    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }

    public static ArrayList<Alumno> llenarAlumnos (){
        ArrayList<Alumno> alumnos = new ArrayList<>();
        String carrera = "Ing. Tec. Información";

        /*alumnos.add(new Alumno(carrera, "MORUA ZAMUDIO ESTEFANO", R.drawable.ic_2019030344, "2019030344"));
        alumnos.add(new Alumno(carrera, "CARRANZA JAUREGUI CARLOS ALBERTO", R.drawable.ic_2020030174, "2020030174"));
        alumnos.add(new Alumno(carrera, "CASTRO LOPEZ MARCO ANTONIO ALARID", R.drawable.ic_2020030176, "202030176"));
        alumnos.add(new Alumno(carrera, "DURAN VALDEZ JOSHUA DANIEL", R.drawable.ic_2020030181, "2020030181"));
        alumnos.add(new Alumno(carrera, "GALINDO HERNANDEZ ERNESTO DAVID", R.drawable.ic_2020030184, "2020030184"));
        alumnos.add(new Alumno(carrera, "CONTRERAS CEPEDA MAXIMILIANO", R.drawable.ic_2020030189, "2020030189"));
        alumnos.add(new Alumno(carrera, "GOMEZ RUELAS IVÁN ENRIQUE", R.drawable.ic_2020030199, "2020030199"));
        alumnos.add(new Alumno(carrera, "CRUZ QUINTERO JESUS EDUARDO", R.drawable.ic_2020030212, "2020030212"));
        alumnos.add(new Alumno(carrera, "VELARDE OVALLE DAVID ANTONIO", R.drawable.ic_2020030241, "2020030241"));
        alumnos.add(new Alumno(carrera, "LAMAS ARMENTA GUSTAVO ADOLFO", R.drawable.ic_2019030344, "2020030243"));
        alumnos.add(new Alumno(carrera, "RIVAS LUGO JUAN CARLOS", R.drawable.ic_2020030249, "2020030249"));
        alumnos.add(new Alumno(carrera, "SALAS MENDOZA ALEJO", R.drawable.ic_2020030264, "2020030264"));
        alumnos.add(new Alumno(carrera, "SERRANO TORRES CARLOS JAIR", R.drawable.ic_2020030268, "2020030268"));
        alumnos.add(new Alumno(carrera, "TIRADO ROMERO JESUS TADEO", R.drawable.ic_2020030292, "2020030292"));
        alumnos.add(new Alumno(carrera, "CARRILLO GARCIA JAIR", R.drawable.ic_2020030304, "2020030304"));
        alumnos.add(new Alumno(carrera, "ARIAS ZATARAIN DIEGO", R.drawable.ic_2020030306, "2020030306"));
        alumnos.add(new Alumno(carrera, "VALDEZ MARTINEZ PAOLA EMIRET", R.drawable.ic_2020030313, "2020030313"));
        alumnos.add(new Alumno(carrera, "IBARRA FLORES SALMA YARETH", R.drawable.ic_2020030315, "2020030315"));
        alumnos.add(new Alumno(carrera, "LIZARRAGA MALDONADO JUAN ANTONIO", R.drawable.ic_2020030322, "2020030322"));
        alumnos.add(new Alumno(carrera, "VIERA ROMERO ANGEL ZINEDINE ANASTACIO", R.drawable.ic_2020030325, "2020030325"));
        alumnos.add(new Alumno(carrera, "TEJEDA PEINADO BLAS ALBERTO", R.drawable.ic_2020030327, "2020030327"));
        alumnos.add(new Alumno(carrera, "VIERA ROMERO ANGEL RONALDO ANASTACIO", R.drawable.ic_2020030329, "2020030329"));
        alumnos.add(new Alumno(carrera, "ELIZALDE VARGAS XIOMARA YAMILETH", R.drawable.ic_2020030332, "2020030332"));
        alumnos.add(new Alumno(carrera, "SALCIDO SARABIA JESUS ANTONIO", R.drawable.ic_2020030333, "2020030333"));
        alumnos.add(new Alumno(carrera, "RODRIGUEZ SANCHEZ YENNIFER CAROLINA", R.drawable.ic_2020030389, "2020030389"));
        alumnos.add(new Alumno(carrera, "FLORES PRADO MANUEL ALEXIS", R.drawable.ic_2020030766, "2020030766"));
        alumnos.add(new Alumno(carrera, "AGUIRRE TOSTADO VICTOR MOISES", R.drawable.ic_2020030771, "2020030771"));
        alumnos.add(new Alumno(carrera, "DOMINGUEZ SARABIA HALACH UINIC", R.drawable.ic_2020030777, "2020030777"));
        alumnos.add(new Alumno(carrera, "MACIEL NUÑEZ ENZO ALEJANDRO", R.drawable.ic_2020030799, "2020030799"));
        alumnos.add(new Alumno(carrera, "BARRON VARGAS JOSE ALBERTO", R.drawable.ic_2020030808, "2020030808"));
        alumnos.add(new Alumno(carrera, "MARTIN IBARRA GIANCARLO", R.drawable.ic_2020030819, "2020030819"));
        alumnos.add(new Alumno(carrera, "SANCHEZ OCEGUEDA LUIS ANGEL", R.drawable.ic_2020030865, "2020030865"));*/

        return alumnos;
    }
}


