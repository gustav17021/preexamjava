package com.example.listview92;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class MiAdaptador extends RecyclerView.Adapter<MiAdaptador.ViewHolder> implements View.OnClickListener {
    private Application context;
    private ArrayList<Alumno> alumno;
    private ArrayList<Alumno> listaAlumnos;
    private LayoutInflater inflater;
    private View.OnClickListener listener;

    public MiAdaptador(ArrayList<Alumno> listaAlumnos, Application context) {
        this.listaAlumnos = listaAlumnos;
        this.alumno = new ArrayList<>(listaAlumnos);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.alumnos_items, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Alumno alumno = listaAlumnos.get(position);
        if (alumno != null) {
            holder.txtCarrera.setText(alumno.getCarrera());
            holder.txtNombre.setText(alumno.getNombre());
            Glide.with(holder.itemView.getContext())
                    .load(alumno.getImagenUrl())
                    .placeholder(R.drawable.agregar_alumno)
                    .error(R.drawable.close)
                    .into(holder.idImagen);
            holder.txtMatricula.setText(alumno.getMatricula());
        }
    }

    @Override
    public int getItemCount() {
        return listaAlumnos.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();

                ArrayList<Alumno> filteredList = new ArrayList<>();

                if (charSequence != null && charSequence.length() > 0) {
                    String searchQuery = charSequence.toString().toLowerCase().trim();

                    for (Alumno item : alumno) {
                        String nombre = item.getNombre().toLowerCase().trim();
                        String matricula = item.getMatricula().toLowerCase().trim();

                        if (nombre.contains(searchQuery) || matricula.contains(searchQuery)) {
                            filteredList.add(item);
                        }
                    }
                } else {
                    filteredList.addAll(alumno);
                }

                results.values = filteredList;
                results.count = filteredList.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                listaAlumnos.clear();
                listaAlumnos.addAll((List<Alumno>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNombre = itemView.findViewById(R.id.txtAlumnoNombre);
            txtMatricula = itemView.findViewById(R.id.txtMatricula);
            txtCarrera = itemView.findViewById(R.id.txtCarrera);
            idImagen = itemView.findViewById(R.id.foto);
        }
    }

    public void setAlumnoList(ArrayList<Alumno> listaAlumnos) {
        this.alumno.clear();
        this.alumno.addAll(listaAlumnos);
        notifyDataSetChanged();
    }
}
