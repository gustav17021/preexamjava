package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class AlumnosDb implements Persistencia, Proyeccion{
    private Context context;
    private AlumnoDbHelper helper;
    private SQLiteDatabase db;

    public AlumnosDb(Context context, AlumnoDbHelper helper){
        this.context = context;
        this.helper = helper;
    }
    public AlumnosDb(Context context){
        this.context = context;
        this.helper = new AlumnoDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();

    }

    @Override
    public long insertAlumno(Alumno alumno) {
        ContentValues values = new ContentValues();
        values.put(DefineTabla.Alumnos.COLUM_NAME_MATRICULA, alumno.getMatricula());
        values.put(DefineTabla.Alumnos.COLUMN_NAME_NOMBRE, alumno.getNombre());
        values.put(DefineTabla.Alumnos.COLUMN_NAME_CARRERA, alumno.getGrado());
        values.put(DefineTabla.Alumnos.COLUMN_NAME_FOTO, alumno.getImg());


        this.openDataBase();
        long num = db.insert(DefineTabla.Alumnos.TABLE_NAME,null, values);
        this.closeDataBase();
        Log.d(tag: "agregar",msg: "insertAlumno" + num);
        return num;
    }

    @Override
    public long updateAlumno(Alumno alumno) {
        ContentValues values = new ContentValues();
        values.put(DefineTabla.Alumnos.COLUM_NAME_MATRICULA, alumno.getMatricula());
        values.put(DefineTabla.Alumnos.COLUMN_NAME_NOMBRE, alumno.getNombre());
        values.put(DefineTabla.Alumnos.COLUMN_NAME_CARRERA, alumno.getGrado());
        values.put(DefineTabla.Alumnos.COLUMN_NAME_FOTO, alumno.getImg());


        this.openDataBase();
        long num = db.update(
                DefineTabla.Alumnos.TABLE_NAME,
                values,
                whereClause: DefineTabla.Alumnos.COLUM_NAME_ID + alumno.getId(),
                whereArgs: null);
        )
        this.closeDataBase();
        return num;
    }

    @Override
    public void deleteAlumnos(int id) {
        this.openDataBase();
        db.delete(
                DefineTabla.Alumnos.TABLE_NAME,
                whereClause: DefineTabla.Alumnos.COLUM_NAME_ID + "=?",
                new String[] {String.valueOf(id)} );
        this.closeDataBase();



    }

   @Override
    public Alumno getAlumno(String matricula) {

        db.helper.getWritableDatabase();
        Cursor cursor = db.query(
                DefineTabla.Alumnos.TABLE_NAME,
                DefineTabla.Alumnos.REGISTRO,
                selection: DefineTabla.Alumnos.COLUM_NAME_ID + " = ?",
                new String [] {matricula},
                groupBy: null, having null, orderBy: null);
        cursor.moveToFirst();
        Alumno alumno = readAlumno(cursor);
        return null;
    }

    @Override
    public ArrayList<Alumno> allAlumnos() {
        db.helper.getWritableDatabase();
        Cursor cursor = db.query(
                DefineTabla.Alumnos.TABLE_NAME,
                DefineTabla.Alumnos.REGISTRO,
                selection: null, selectionArgs: null, groupBy; null, having: null, orderBy: null);
        ArrayList<Alumno>Alumnos = new ArrayList<Alumno>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Alumno alumno = readAlumno(cursor);
            alumnos.add(alumno);
            cursor.moveToNext();
        }
        cursor.close();
        return alumnos;
    }

    @Override
    public Alumno readAlumno(Cursor cursor) {
        Alumno alumno = new Alumno();
        alumno.setId(cursor.getInt(columnIndex: 0));
        alumno.setMatricula(cursor.getString(columnIndex: 1));
        alumno.setNombre(cursor.getString(columnIndex: 2));
        alumno.setGrado(cursor.getString(columnIndex: 3));

        return alumno;
    }
}
