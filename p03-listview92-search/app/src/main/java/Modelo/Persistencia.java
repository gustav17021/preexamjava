package Modelo;

import com.example.listview92.AlumnoItem;
public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertAlumno(Alumno alumno);
    public long updateAlumno(Alumno alumno);
    public void deleteAlumnos(int id);

}
