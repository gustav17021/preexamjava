package com.example.exac2_serrano_torres;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import Modelo.VentaDb;

public class VentasActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton btnRegresar;
    private Aplicacion app;
    private Ventas venta;
    private int posicion;
    private VentaDb ventaDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventas);

        inicializarComponentes();
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRegresar();
            }
        });
        app.getAdaptador().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView(view);
            }
        });
    }

    private void inicializarComponentes() {
        venta = new Ventas();
        posicion = -1;
        app = (Aplicacion) getApplication();
        layoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.recId);
        recyclerView.setAdapter(app.getAdaptador());
        recyclerView.setLayoutManager(layoutManager);

        btnRegresar = findViewById(R.id.btnRegresar);
        ventaDb = new VentaDb(getApplicationContext());
    }

    private void btnRegresar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmación");
        builder.setMessage("¿Estás seguro de que quieres regresar?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Regresar al MainActivity
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void recyclerView(View view) {
        posicion = recyclerView.getChildAdapterPosition(view);
        venta = app.getVentas().get(posicion);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == Activity.RESULT_OK) {
            app.getVentas().clear();
            app.getVentas().addAll(ventaDb.allVentas());
            app.getAdaptador().setVentaList(app.getVentas());
            recreate();
            app.getAdaptador().setVentaList(app.getVentas());
        }

        this.posicion = -1;
        this.venta = null;
    }
}