package com.example.exac1_2020030181;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class cuentaBancoActivity extends AppCompatActivity {
    private TextView lblBanco;
    private TextView lblNombreC;
    private  TextView lblSaldo;
    private EditText txtCant;
    private Button btnDepositar;
    private Button btnRetirar;
    private Button btnRegresar;
    private CuentaBanco cuentaBanco = new CuentaBanco(0, "", "", 0.0);

    public void cuentaBancoActivity(){
        //vacio
    }
    public void cuentaBancoActivity(cuentaBancoActivity cba){
        this.lblBanco = cba.lblBanco;
        this.lblNombreC = cba.lblNombreC;
        this. lblSaldo = cba.lblSaldo;
        this.txtCant = cba.txtCant;
        this.btnDepositar = cba.btnDepositar;
        this.btnRetirar = cba.btnRetirar;
        this.btnRegresar = cba.btnRegresar;
    }
    public void cuentaBancoActivity(TextView lblBanco, TextView lblNombreC, TextView lblSaldo, EditText txtCant, Button btnDepositar, Button btnRetirar, Button btnRegresar){
        this.lblBanco = lblBanco;
        this.lblNombreC = lblNombreC;
        this.lblSaldo = lblSaldo;
        this.txtCant = txtCant;
        this.btnDepositar = btnDepositar;
        this.btnRetirar = btnRetirar;
        this.btnRegresar = btnRegresar;
    }

    public TextView getLblBanco() {
        return lblBanco;
    }

    public void setLblBanco(TextView lblBanco) {
        this.lblBanco = lblBanco;
    }

    public TextView getLblNombreC() {
        return lblNombreC;
    }

    public void setLblNombreC(TextView lblNombreC) {
        this.lblNombreC = lblNombreC;
    }

    public TextView getLblSaldo() {
        return lblSaldo;
    }

    public void setLblSaldo(TextView lblSaldo) {
        this.lblSaldo = lblSaldo;
    }

    public EditText getTxtCant() {
        return txtCant;
    }

    public void setTxtCant(EditText txtCant) {
        this.txtCant = txtCant;
    }

    public Button getBtnDepositar() {
        return btnDepositar;
    }

    public void setBtnDepositar(Button btnDepositar) {
        this.btnDepositar = btnDepositar;
    }

    public Button getBtnRetirar() {
        return btnRetirar;
    }

    public void setBtnRetirar(Button btnRetirar) {
        this.btnRetirar = btnRetirar;
    }

    public Button getBtnRegresar() {
        return btnRegresar;
    }

    public void setBtnRegresar(Button btnRegresar) {
        this.btnRegresar = btnRegresar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);
        iniciarComponentes();
        Bundle datos = getIntent().getExtras();
        String banco = datos.getString("banco");
        String cliente = datos.getString("nombre");
        Double saldo = datos.getDouble("saldo");
        lblNombreC.setText(cliente.toString());
        lblBanco.setText(banco.toString());
        lblSaldo.setText((saldo.toString()));
        cuentaBanco.setSaldo(saldo);

        btnDepositar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { depositar(); }
        });
        btnRetirar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { retirar(); }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { salir(); }
        });
    }

    private void iniciarComponentes(){
        this.lblBanco = findViewById(R.id.lblBanco);
        this.lblNombreC = findViewById(R.id.lblNombreC);
        this.lblSaldo = findViewById(R.id.lblSaldo);
        this.txtCant = findViewById(R.id.txtCant);
        this.btnDepositar = findViewById(R.id.btnDepositar);
        this.btnRetirar = findViewById(R.id.btnRetirar);
        this.btnRegresar = findViewById(R.id.btnRegresar);
    }
    private void depositar(){
        if(this.txtCant.getText().toString().equals("") || Double.valueOf(this.txtCant.getText().toString()) <= 0){
            Toast.makeText(getApplicationContext(), "Ingrese una cantidad valida para depositar.", Toast.LENGTH_SHORT).show();
        }else{
            cuentaBanco.depositar(Double.valueOf(this.txtCant.getText().toString()));
            this.lblSaldo.setText(cuentaBanco.getSaldo().toString());
        }
    }
    private void retirar(){
        if(this.txtCant.getText().toString().equals("") || Double.valueOf(this.txtCant.getText().toString()) <= 0){
            Toast.makeText(getApplicationContext(), "Ingrese una cantidad valida para depositar.", Toast.LENGTH_SHORT).show();
        }else if(Double.valueOf(this.txtCant.getText().toString()) > Double.valueOf(lblSaldo.getText().toString())){
            Toast.makeText(getApplicationContext(), "Saldo insuficiente, usted cuenta con un saldo de: $"+ lblSaldo.getText().toString() +".", Toast.LENGTH_SHORT).show();
        }else{
            cuentaBanco.retirar(Double.valueOf(txtCant.getText().toString()));
            this.lblSaldo.setText(cuentaBanco.getSaldo().toString());
        }
    }
    private void salir(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Cuenta bancaria");
        confirmar.setMessage("Desea regresar al inicio?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }
}