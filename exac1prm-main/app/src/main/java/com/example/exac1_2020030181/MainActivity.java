package com.example.exac1_2020030181;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNC;
    private EditText txtNombre;
    private EditText txtBanco;
    private EditText txtSaldo;
    private Button btnEnviar;
    private Button btnSalir;

    public MainActivity(){
        //vacio
    }
    public MainActivity(MainActivity ma){
        this.txtNC = ma.txtNC;
        this.txtNombre = ma.txtNombre;
        this.txtBanco = ma.txtBanco;
        this.txtSaldo = ma.txtSaldo;
        this.btnEnviar = ma.btnEnviar;
        this.btnSalir = ma.btnSalir;
    }
    public MainActivity(EditText txtNC, EditText txtNombre, EditText txtBanco, Button btnEnviar, Button btnSalir){
        this.txtNC = txtNC;
        this.txtNombre = txtNombre;
        this.txtBanco = txtBanco;
        this.btnEnviar = btnEnviar;
        this.btnSalir = btnSalir;
    }

    public EditText getTxtNC() {
        return txtNC;
    }

    public void setTxtNC(EditText txtNC) {
        this.txtNC = txtNC;
    }

    public EditText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(EditText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public EditText getTxtBanco() {
        return txtBanco;
    }

    public void setTxtBanco(EditText txtBanco) {
        this.txtBanco = txtBanco;
    }

    public EditText getTxtSaldo() {
        return txtSaldo;
    }

    public void setTxtSaldo(EditText txtSaldo) {
        this.txtSaldo = txtSaldo;
    }

    public Button getBtnEnviar() {
        return btnEnviar;
    }

    public void setBtnEnviar(Button btnEnviar) {
        this.btnEnviar = btnEnviar;
    }

    public Button getBtnSalir() {
        return btnSalir;
    }

    public void setBtnSalir(Button btnSalir) {
        this.btnSalir = btnSalir;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { ingresar(); }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { salir(); }
        });
    }
    private void iniciarComponentes(){
        txtNC = findViewById(R.id.txtNC);
        txtNombre = findViewById(R.id.txtNombre);
        txtBanco = findViewById(R.id.txtBanco);
        txtSaldo = findViewById(R.id.txtSaldo);
        btnEnviar = findViewById(R.id.btnEnviar);
        btnSalir = findViewById(R.id.btnSalir);
    }
    private void ingresar(){
        if(txtNC.getText().toString().equals("") || txtNombre.getText().toString().equals("") || txtSaldo.getText().toString().equals("") || txtBanco.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Ingrese todos los datos para ingresar.", Toast.LENGTH_SHORT).show();
        }else{
            Bundle bundle = new Bundle();
            bundle.putString("numerocuenta", txtNC.getText().toString());
            bundle.putString("nombre", txtNombre.getText().toString());
            bundle.putString("banco", txtBanco.getText().toString());
            bundle.putDouble("saldo", Double.valueOf(txtSaldo.getText().toString()));
            Intent intent = new Intent(MainActivity.this, cuentaBancoActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        }
    }
    private void salir(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Inicio");
        confirmar.setMessage("Desea salir?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }
}