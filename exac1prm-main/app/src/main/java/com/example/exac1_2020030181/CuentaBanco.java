package com.example.exac1_2020030181;

public class CuentaBanco {
    private int numCuenta = 0;
    private String nombre = "";
    private String banco = "";
    private Double saldo = 0.0;

    public CuentaBanco(){
        //constructor vacío
    }
    public CuentaBanco(CuentaBanco cb){
        this.numCuenta = cb.numCuenta;
        this.nombre = cb.nombre;
        this.banco = cb.banco;
        this.saldo = cb.saldo;
    }
    public CuentaBanco(int numCuenta, String nombre, String banco, Double saldo){
        this.numCuenta = numCuenta;
        this.nombre = nombre;
        this.banco = banco;
        this.saldo = saldo;
    }

    public int getNumCuenta() { return numCuenta; }

    public void setNumCuenta(int numCuenta) { this.numCuenta = numCuenta; }

    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getBanco() { return banco; }

    public void setBanco(String banco) { this.banco = banco; }

    public Double getSaldo() { return saldo; }

    public void setSaldo(Double saldo) { this.saldo = saldo; }

    public void depositar(Double cantDepositar){
        Double saldoAct = this.getSaldo();
        setSaldo(saldoAct + cantDepositar);
    }
    public boolean retirar(Double cantRetirar){
        Double saldoAct = this.getSaldo();
        if(cantRetirar > saldo) {
            return false;
        }else{
            setSaldo(saldoAct - cantRetirar);
            return true;
        }
    }
}
