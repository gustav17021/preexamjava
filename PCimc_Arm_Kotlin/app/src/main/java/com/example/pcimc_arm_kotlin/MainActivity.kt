package com.example.pcimc_arm_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.text.DecimalFormat


class MainActivity : AppCompatActivity() {
    private lateinit var Peso: EditText
    private lateinit var Altura: EditText
    private lateinit var Imc: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun Calcular(v: View) {
        Peso = findViewById(R.id.lblPeso)
        Altura = findViewById(R.id.lblAltura)
        Imc = findViewById(R.id.lblIMC)

        if (Va(Peso.text.toString(), Altura.text.toString())) {
            ins(Co(Peso.text.toString()), Co(Altura.text.toString()))
        }
    }

    fun Limpiar(v: View) {
        Peso = findViewById(R.id.lblPeso)
        Altura = findViewById(R.id.lblAltura)
        Imc = findViewById(R.id.lblIMC)

        Peso.text.clear()
        Altura.text.clear()
        Imc.text = ""
    }

    fun Cerrar(v: View) {
        finish()
    }

    private fun Va(peso: String, altura: String): Boolean {
        if (peso.isBlank() || altura.isBlank()) {
            Toast.makeText(this, "Completa todos los campos", Toast.LENGTH_SHORT).show()
        }  else {
            return true
        }
        return false
    }



    private fun Co(aop: String): Int {
        return aop.toInt()
    }

    private fun ins(peso: Int, altura: Int) {
        Imc = findViewById(R.id.lblIMC)
        val df = DecimalFormat("#.00")
        val form = peso / ((altura / 100.0) * (altura / 100.0))
        val resultado = String.format("%.2f", form)
        Imc.text = "$resultado kg/m²"
    }
}