package com.example.examencorte2_92;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import Modelo.VentaDb;

public class ListaActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private RecyclerView listaVentas;
    private TextView lblTitulo, lblSubtitulo, total;
    private Button btnRegresar;
    private Aplicacion app;
    private Venta venta;
    private VentaDb ventaDb;
    private ArrayList<Venta> listaVenta;
    private int posicion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        inicializarComponentes();

        total = findViewById(R.id.txtTotal);
        ventaDb = new VentaDb(getApplicationContext());
        total.setText("VENTAS TOTALES: "+ String.valueOf(ventaDb.sumarTotalesVentas()));
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRegresar();
            }
        });
        app.getAdaptador().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView(view);
            }
        });
    }

    private void inicializarComponentes() {
        venta = new Venta();
        posicion = -1;
        app = (Aplicacion) getApplication();
        layoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.listaVentas);
        recyclerView.setAdapter(app.getAdaptador());
        recyclerView.setLayoutManager(layoutManager);

        btnRegresar = findViewById(R.id.btnRegresar);
        ventaDb = new VentaDb(getApplicationContext());
    }

    private void btnRegresar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmación");
        builder.setMessage("¿Estás seguro de que quieres regresar?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Regresar al MainActivity
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void recyclerView(View view) {
        posicion = recyclerView.getChildAdapterPosition(view);
        venta = app.getVentas().get(posicion);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == Activity.RESULT_OK) {
            app.getVentas().clear();
            app.getVentas().addAll(ventaDb.allVentas());
            app.getAdaptador().setVentaList(app.getVentas());
            recreate();
            app.getAdaptador().setVentaList(app.getVentas());
        }

        this.posicion = -1;
        this.venta = null;
    }
}
