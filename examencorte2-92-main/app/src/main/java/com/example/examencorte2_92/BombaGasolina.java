package com.example.examencorte2_92;

import java.io.Serializable;

public class BombaGasolina implements Serializable {
    private int id;
    private int numBomba;
    private int capacidadBomba;
    private int tipoGasolina;
    private float precio;
    private int acumuladorLitrosBomba;

    public BombaGasolina() {
        this.numBomba = 0;
        this.capacidadBomba = 0;
        this.tipoGasolina = 0;
        this.precio = 0.0f;
        this.acumuladorLitrosBomba = 0;
    }

    public BombaGasolina(int numBomba, int capacidadBomba, int tipoGasolina, float precio, int acumuladorLitrosBomba) {
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.tipoGasolina = tipoGasolina;
        this.precio = precio;
        this.acumuladorLitrosBomba = acumuladorLitrosBomba;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public int getCapacidadBomba() {
        return capacidadBomba;
    }

    public void setCapacidadBomba(int capacidadBomba) {
        this.capacidadBomba = capacidadBomba;
    }

    public int getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(int tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getAcumuladorLitrosBomba() {
        return acumuladorLitrosBomba;
    }

    public void setAcumuladorLitrosBomba(int acumuladorLitrosBomba) {
        this.acumuladorLitrosBomba = acumuladorLitrosBomba;
    }
    public float hacerventa(int cantidad){
        this.acumuladorLitrosBomba = acumuladorLitrosBomba + cantidad;
        return (cantidad * this.precio);
    }
}
