package com.example.examencorte2_92;

import java.io.Serializable;

public class Venta implements Serializable {
    private long id;
    private int numBomba;
    private int tipoGasolina;
    private float precio;
    private int cantidad;
    private float total;

    public Venta() {
        this.numBomba = 0;
        this.tipoGasolina = 0;
        this.precio = 0.0f;
        this.cantidad = 0;
        this.total = 0.0f;
    }

    public Venta(int numBomba, int tipoGasolina, float precio, int cantidad, float total) {
        this.numBomba = numBomba;
        this.tipoGasolina = tipoGasolina;
        this.precio = precio;
        this.cantidad = cantidad;
        this.total = total;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public int getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(int tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
