package com.example.examencorte2_92;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class VentaAdapter extends RecyclerView.Adapter<VentaAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<Venta> listaVentas;
    private Application context;
    private ArrayList<Venta> ventas;
    private LayoutInflater inflater;
    private View.OnClickListener listener;

    public VentaAdapter(ArrayList<Venta> listaVentas, Application context) {
        this.listaVentas = listaVentas;
        this.ventas = new ArrayList<>(listaVentas);
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.venta_item, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Venta venta = listaVentas.get(position);
        if (venta != null) {
            holder.txtCantidad.setText("Cantidad: " + venta.getCantidad());
            holder.txtPrecio.setText("Precio: " + venta.getPrecio());
            holder.txtTotal.setText("Total: " + venta.getTotal());
        }
    }

    @Override
    public int getItemCount() {
        return listaVentas.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtCantidad;
        private TextView txtPrecio;
        private TextView txtTotal;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCantidad = itemView.findViewById(R.id.txtCantidad);
            txtPrecio = itemView.findViewById(R.id.txtPrecio);
            txtTotal = itemView.findViewById(R.id.txtTotal);
        }
    }
    public void setVentaList(ArrayList<Venta> listaVentas) {
        this.ventas.clear();
        this.ventas.addAll(listaVentas);
        notifyDataSetChanged();
    }
}
