package com.example.examencorte2_92;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.examencorte2_92.BombaGasolina;
import com.example.examencorte2_92.Venta;

import java.util.ArrayList;

import Modelo.VentaDb;

public class MainActivity extends AppCompatActivity {

    private TextView lblIniBomba, lblTablaVenta, lblTablaRes;
    private EditText txtNumBomba, txtPrecio, txtCapBomba, txtContLitros, txtCantidad;
    private RadioGroup rdbTipoBomba;
    private Button btnIniciarBomba, btnHacerVenta, btnRegistrarVenta, btnConsultarVentas, btnLimpiar, btnSalir;
    private TableLayout tablaVentas;

    private BombaGasolina bombaGasolina;
    private double totalVentas;

    private VentaDb ventaDb;
    private ArrayList<Venta> listaVentas;
    private Venta venta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ventaDb = new VentaDb(this);

        inicializarComponentes();

        btnIniciarBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciarVenta();
            }
        });

        btnHacerVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hacerVenta();
            }
        });

        btnRegistrarVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarVenta();
            }
        });

        btnConsultarVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarRegistros();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrar();
            }
        });
    }

    private void inicializarComponentes() {
        lblIniBomba = findViewById(R.id.lblIniBomba);
        lblTablaVenta = findViewById(R.id.lblTablaVenta);
        lblTablaRes = findViewById(R.id.lblTablaRes);
        txtNumBomba = findViewById(R.id.txtNumBomba);
        txtPrecio = findViewById(R.id.txtPrecio);
        txtCapBomba = findViewById(R.id.txtCapBomba);
        txtContLitros = findViewById(R.id.txtContLitros);
        txtCantidad = findViewById(R.id.txtCantidad);
        rdbTipoBomba = findViewById(R.id.rdbTipoBomba);
        btnIniciarBomba = findViewById(R.id.btnIniciarBomba);
        btnHacerVenta = findViewById(R.id.btnHacerVenta);
        btnRegistrarVenta = findViewById(R.id.btnRegistrarVenta);
        btnConsultarVentas = findViewById(R.id.btnConsultarVenta);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);

        listaVentas = new ArrayList<>();
        bombaGasolina = new BombaGasolina();
    }

    /*private void iniciarVenta() {
        int numBomba = Integer.parseInt(txtNumBomba.getText().toString());
        int tipoGasolina = rdbTipoBomba.getCheckedRadioButtonId();
        int capacidadBomba = Integer.parseInt(txtCapBomba.getText().toString());
        float precio = Float.parseFloat(txtPrecio.getText().toString());
        int acumuladorLitrosBomba = Integer.parseInt(txtContLitros.getText().toString());
        bombaGasolina = new BombaGasolina(numBomba, capacidadBomba, tipoGasolina, precio, acumuladorLitrosBomba);
        txtContLitros.setText("0");
        lblIniBomba.setText("Bomba iniciada (Bomba: " + numBomba + ", Tipo: " + tipoGasolina + ")");
    }*/

    private void hacerVenta() {
        String numBomba = txtNumBomba.getText().toString().trim();
        int tipoGasolinaCont = rdbTipoBomba.getCheckedRadioButtonId();
        String precioCont = txtPrecio.getText().toString().trim();
        String cantidadCont = txtCantidad.getText().toString().trim();

        if (numBomba.isEmpty() || tipoGasolinaCont == -1 || precioCont.isEmpty() || cantidadCont.isEmpty()) {
            Toast.makeText(this, "Completa todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        float precioGasolina;
        int cantidad;

        try {
            precioGasolina = Float.parseFloat(precioCont);
            cantidad = Integer.parseInt(cantidadCont);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese un valor numérico válido para el precio y la cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        if (Integer.parseInt(txtContLitros.getText().toString()) == Integer.parseInt(txtCapBomba.getText().toString())) {
            Toast.makeText(this, "Ya se terminó la gasolina", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cantidad > Integer.parseInt(txtCapBomba.getText().toString())) {
            Toast.makeText(this, "No puedes pasar de la gasolina máxima", Toast.LENGTH_SHORT).show();
            return;
        }

        RadioButton selectedRadioButton = findViewById(tipoGasolinaCont);
        int tipoGasolina = 0;

        if (selectedRadioButton.getId() == R.id.rdbRegular) {
            tipoGasolina = 1;
        } else if (selectedRadioButton.getId() == R.id.rdbExtra) {
            tipoGasolina = 2;
        }

        Venta venta = new Venta(Integer.parseInt(numBomba), tipoGasolina, precioGasolina, cantidad, 0);
        float totalPagar = venta.getPrecio() * venta.getCantidad();
        venta.setTotal(totalPagar);

        lblTablaVenta.setText("Cantidad: " + venta.getCantidad() + " Precio: " + venta.getPrecio() + " Total a pagar: " + venta.getTotal());

        this.venta = venta;
        btnRegistrarVenta.setEnabled(true);

        Toast.makeText(this, "Venta realizada", Toast.LENGTH_SHORT).show();
    }

    private void registrarVenta() {
        long resultado = ventaDb.insertVenta(venta);

        if (resultado != -1) {
            Toast.makeText(this, "Venta guardada correctamente", Toast.LENGTH_SHORT).show();
            Aplicacion app = (Aplicacion) getApplication();
            app.getVentas().add(venta);
            int contadorActual = Integer.parseInt(txtContLitros.getText().toString());
            int nuevoContador = (int) (contadorActual + venta.getCantidad());
            txtContLitros.setText(String.valueOf(nuevoContador));
            btnRegistrarVenta.setEnabled(false);
        } else {
            Toast.makeText(this, "Error al guardar la venta", Toast.LENGTH_SHORT).show();
        }

        Toast.makeText(this, "Ventas registradas", Toast.LENGTH_SHORT).show();
    }

    private void mostrarRegistros() {
        Intent intent = new Intent(this, ListaActivity.class);
        startActivityForResult(intent, 0);
        Toast.makeText(this, "Mostrando registros", Toast.LENGTH_SHORT).show();
    }


    private void iniciarVenta() {
        String numBomba = txtNumBomba.getText().toString().trim();
        String capacidadBombaStr = txtCapBomba.getText().toString().trim();
        String precioGasolinaStr = txtPrecio.getText().toString().trim();
        int tipoGasolinaId = rdbTipoBomba.getCheckedRadioButtonId();

        if (numBomba.isEmpty() || capacidadBombaStr.isEmpty() || precioGasolinaStr.isEmpty()) {
            Toast.makeText(this, "Completa todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        int capacidadBomba;
        float precioGasolina;

        try {
            capacidadBomba = Integer.parseInt(capacidadBombaStr);
            precioGasolina = Float.parseFloat(precioGasolinaStr);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese un valor numérico válido para la capacidad de la bomba y el precio de la gasolina", Toast.LENGTH_SHORT).show();
            return;
        }

        RadioButton selectedRadioButton = findViewById(tipoGasolinaId);
        int tipoGasolina = 0;

        if (selectedRadioButton.getId() == R.id.rdbRegular) {
            tipoGasolina = 1;
        } else if (selectedRadioButton.getId() == R.id.rdbExtra) {
            tipoGasolina = 2;
        }

        bombaGasolina.setNumBomba(Integer.parseInt(numBomba));
        bombaGasolina.setCapacidadBomba(capacidadBomba);
        bombaGasolina.setPrecio(precioGasolina);
        bombaGasolina.setTipoGasolina(tipoGasolina);
        bombaGasolina.setAcumuladorLitrosBomba(0);
        txtContLitros.setText("0");
    }

    private void limpiar() {
        txtCantidad.setText("");
        lblTablaVenta.setText("");
    }

    private void cerrar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Confirmación");
        builder.setMessage("¿Estás seguro de que quieres cerrar la aplicación?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }
}