package com.example.examencorte2_92;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.VentaDb;

public class Aplicacion extends Application {

    public static ArrayList<Venta> ventas;
    private VentaAdapter ventaAdapter;
    private VentaDb ventaDb;

    public ArrayList<Venta> getVentas() {
        return ventas;
    }
    public VentaAdapter getAdaptador() {
        return ventaAdapter;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ventaDb = new VentaDb(getApplicationContext());
        ventas = ventaDb.allVentas();
        ventaDb.openDataBase();

        Log.d("", "onCreate: Tamaño array list: " + this.ventas.size());
        this.ventaAdapter = new VentaAdapter(this.ventas, this);
    }

    public void agregarVenta(Venta venta) {
        ventaDb.openDataBase();
        long resultado = ventaDb.insertVenta(venta);
        if (resultado != -1) {
            venta.setId((int) resultado);
            ventas.add(venta);
            ventaAdapter.notifyDataSetChanged();
        }
    }
}

