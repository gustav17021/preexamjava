package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.examencorte2_92.Venta;

import java.util.ArrayList;

public class VentaDb implements Persistencia, Proyeccion {

    private Context context;
    private VentaDbHelper helper;
    private SQLiteDatabase db;

    public VentaDb(Context context, VentaDbHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public VentaDb(Context context) {
        this.context = context;
        this.helper = new VentaDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertVenta(Venta venta) {

        ContentValues values = new ContentValues();
        values.put(DefineTable.Ventas.COLUMN_NAME_NUM_BOMBA, venta.getNumBomba());
        values.put(DefineTable.Ventas.COLUMN_NAME_TIPO_GASOLINA, venta.getTipoGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_PRECIO, venta.getPrecio());
        values.put(DefineTable.Ventas.COLUMN_NAME_CANTIDAD_GASOLINA, venta.getCantidad());
        values.put(DefineTable.Ventas.COLUMN_NAME_TOTAL, venta.getTotal());

        this.openDataBase();
        long num = db.insert(DefineTable.Ventas.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar", "insertVenta: " + num);

        return num;
    }


    public float sumarTotalesVentas() {
        this.openDataBase(); // Abre la base de datos

        Cursor cursor = db.rawQuery("SELECT SUM(" + DefineTable.Ventas.COLUMN_NAME_TOTAL + ") FROM " +
                DefineTable.Ventas.TABLE_NAME, null);

        float total = 0;

        if (cursor != null && cursor.moveToFirst()) {
            total = cursor.getFloat(0);
        }

        cursor.close();
        this.closeDataBase();

        return total;
    }

    @Override
    public Venta getVenta(int numBomba) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                DefineTable.Ventas.COLUMN_NAME_NUM_BOMBA + " = ?",
                new String[]{String.valueOf(numBomba)},
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            Venta venta = readVenta(cursor);
            cursor.close();
            return venta;
        }
        return null;
    }

    @Override
    public ArrayList<Venta> allVentas() {
        this.openDataBase();

        Cursor cursor = db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                null, null, null, null, null);
        ArrayList<Venta> ventas = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Venta venta = readVenta(cursor);
            ventas.add(venta);
            cursor.moveToNext();
        }

        cursor.close();

        this.closeDataBase();
        return ventas;
    }

    @Override
    public Venta readVenta(Cursor cursor) {
        Venta venta = new Venta();
        venta.setNumBomba(cursor.getInt(0));
        venta.setTipoGasolina(cursor.getInt(1));
        venta.setPrecio(cursor.getFloat(2));
        venta.setCantidad(cursor.getInt(3));
        venta.setTotal(cursor.getFloat(4));
        return venta;
    }
}
