package Modelo;

import android.database.Cursor;

import com.example.examencorte2_92.Venta;

import java.util.ArrayList;

public interface Proyeccion {
    Venta getVenta(int numBomba);
    ArrayList<Venta> allVentas();
    Venta readVenta(Cursor cursor);
}
