package Modelo;


import com.example.examencorte2_92.Venta;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertVenta(Venta venta);
}
