package com.example.imc

import)
        setContentView(R.layout.activity_main)

        weightEditText = findViewById(R.id.weightEditText)
        heightEditText = findViewById(R.id.heightEditText)
        resultTextView = findViewById(R.id.resultTextView)
        calculateButton = findViewById(R.id.calculateButton)

        calculateButton.setOnClickListener {
            calculateBMI()
        }
    }

    private fun calculateBMI() {
        val weight = weightEditText.text.toString().toDouble()
        val height = heightEditText.text.toString().toDouble() / 100 // Convert height to meters

        val bmi = weight / (height * height)

        val result = when {
            bmi < 18.5 -> "Bajo peso"
            bmi < 25 -> "Normal"
            bmi < 30 -> "Sobrepeso"
            else -> "Obeso"
        }

        resultTextView.text = "IMC: $bmi\n$result"
    }
}
