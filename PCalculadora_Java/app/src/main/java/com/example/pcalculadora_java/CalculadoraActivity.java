package com.example.pcalculadora_java;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private TextView lblUsuario;
    private TextView lblResultado;
    private EditText txtUno;
    private EditText txtDos;

    private Calculadora calculadora;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComp();

        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString(MainActivity.EXTRA_MESSAGE);
        lblUsuario.setText(usuario);

        calculadora = new Calculadora(0, 0);
    }
    private void iniciarComp() {

        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);
        txtUno = findViewById(R.id.txtNum1);
        txtDos = findViewById(R.id.txtNum2);
    }
    public boolean vacio() {
        if (txtUno.getText().toString().equals("") || txtDos.getText().toString().equals("")){
            return true;
        }
        else {return false;}
    }
    public void sumar(View v) {
        if (vacio()){
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
        }
        else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.suma();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void resta(View v) {
        if (vacio()){
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
        }
        else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.resta();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void multi(View v) {
        if (vacio()){
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
        }
        else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.multiplicacion();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void div(View v) {
        if (vacio()){
            Toast.makeText(this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
        }
        else {
            calculadora.setNum1(Integer.parseInt(txtUno.getText().toString()));
            calculadora.setNum2(Integer.parseInt(txtDos.getText().toString()));
            int total = calculadora.division();
            lblResultado.setText("Resultado: " + total);
        }
    }

    public void limpiar(View v) {
        lblResultado.setText("Resultado");
        txtUno.setText("");
        txtDos.setText("");
    }

    public void regresar(View v) {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar a el login?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }
}