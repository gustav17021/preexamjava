package com.armenta.banconacional;

public class SolicitanteEspera {
    private String nombre;
    private String rfc;
    private String fecha;
    private double importeAprobado;

    public SolicitanteEspera(String nombre, String rfc,String fecha, double importeAprobado) {
        this.nombre = nombre;
        this.rfc = rfc;
        this.fecha=fecha;
        this.importeAprobado = importeAprobado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }



    public String getNombre() {
        return nombre;
    }

    public String getRfc() {
        return rfc;
    }

    public double getImporteAprobado() {
        return importeAprobado;
    }
}

