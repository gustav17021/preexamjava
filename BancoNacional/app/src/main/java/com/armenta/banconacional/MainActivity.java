package com.armenta.banconacional;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<SolicitanteAprobado> solicitantesAprobados = new ArrayList<>();

    private EditText editTextNombre;
    private EditText editTextRFC;
    private EditText editTextFecha;
    private EditText editTextEdad;
    private EditText editTextIngresos;
    private EditText editTextImporte;
    private Button buttonEnviar;
    private TextView textViewResultado;
    private LinearLayout linearLayoutAprobados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextNombre = findViewById(R.id.editTextNombre);
        editTextRFC = findViewById(R.id.editTextRFC);
        editTextFecha = findViewById(R.id.editTextFecha);
        editTextEdad = findViewById(R.id.editTextEdad);
        editTextIngresos = findViewById(R.id.editTextIngresos);
        editTextImporte = findViewById(R.id.editTextImporte);
        buttonEnviar = findViewById(R.id.buttonEnviar);
        textViewResultado = findViewById(R.id.textViewResultado);
        linearLayoutAprobados = findViewById(R.id.linearLayoutAprobados);


        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = editTextNombre.getText().toString();
                String rfc = editTextRFC.getText().toString();
                String fecha = editTextFecha.getText().toString();
                int edad = Integer.parseInt(editTextEdad.getText().toString());
                double ingresosMensuales = Double.parseDouble(editTextIngresos.getText().toString());
                double importeSolicitado = Double.parseDouble(editTextImporte.getText().toString());

                boolean tieneHis = verificarHistorialCrediticio(rfc);

                double importeMaximo = calcularImporteMaximo(ingresosMensuales, tieneHis);
                if (importeSolicitado <= importeMaximo) {
                    solicitantesAprobados.add(new SolicitanteAprobado(nombre, rfc, fecha, importeSolicitado));
                    textViewResultado.setText("La solicitud ha sido aprobada. Se aprueba un monto de $" + importeSolicitado + " al cliente.");
                    mostrarAprobados();
                } else {
                    if (!tieneHis) {
                        textViewResultado.setText("Solicitud rechazada. No tiene historial y el importe solicitado excede el máximo permitido.");
                    } else {
                        textViewResultado.setText("Solicitud rechazada. El importe solicitado excede el máximo permitido.");
                    }
                }
            }
            private void nuevo(){


            }
            private boolean verificarHistorialCrediticio(String rfc) {
                for (SolicitanteAprobado solicitante : solicitantesAprobados) {
                    if (solicitante.getRfc().equals(rfc)) {
                        return true;
                    }
                }
                return false;
            }

            private boolean verificarEdad(int edad) {
                if (edad > 18) {
                    textViewResultado.setText("Solicitud rechazada. El solicitante es menor de edad.");
                    return true;
                } else {
                    return false;
                }

            }

            private void mostrarAprobados() {
                linearLayoutAprobados.removeAllViews();

                for (SolicitanteAprobado solicitante : solicitantesAprobados) {
                    TextView textView = new TextView(MainActivity.this);
                    textView.setText("Nombre: " + solicitante.getNombre() + "\n" +
                            "RFC: " + solicitante.getRfc() + "\n" +
                            "Fecha de solicitud: " + solicitante.getFecha() + "\n" +
                            "Importe aprobado: " + solicitante.getImporteAprobado() + "\n" +"-----------------------------");
                    linearLayoutAprobados.addView(textView);
                }
            }

            private double calcularImporteMaximo(double ingresosMensuales, boolean tieneHis) {
                double importeMaximo = 0;
                if (tieneHis) {
                    if (ingresosMensuales >= 5000 && ingresosMensuales <= 10000) {
                        importeMaximo = 15000;
                    } else if (ingresosMensuales >= 10000 && ingresosMensuales <= 19999) {
                        importeMaximo = 25000;
                    } else if (ingresosMensuales >= 20000 && ingresosMensuales <= 39999) {
                        importeMaximo = 50000;
                    } else if (ingresosMensuales >= 40000) {
                        importeMaximo = 100000;
                    }
                } else {
                    if (ingresosMensuales >= 5000 && ingresosMensuales <= 10000) {
                        importeMaximo = 7500;
                    } else if (ingresosMensuales >= 10000 && ingresosMensuales <= 19999) {
                        importeMaximo = 12000;
                    } else if (ingresosMensuales >= 20000 && ingresosMensuales <= 39999) {
                        importeMaximo = 30000;
                    } else if (ingresosMensuales >= 40000) {
                        importeMaximo = 50000;
                    }
                }

                return importeMaximo;
            }
        });
    }





}